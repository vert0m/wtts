﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace WTTS.Api.Repositories
{
    public interface IRepository<T> where T : class
    {
        Task<IEnumerable<T>> Get(CancellationToken cancellationToken);
        Task<T> GetById(Guid id, CancellationToken cancellationToken);
        Task<T> Add(T entity, CancellationToken cancellationToken);
        Task Update(T entity, CancellationToken cancellationToken);
        Task Delete(Guid id, CancellationToken cancellationToken);
    }
}
