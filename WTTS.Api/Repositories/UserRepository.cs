﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WTTS.Api.DataAccess;
using WTTS.Api.Entities;

namespace WTTS.Api.Repositories
{
    public interface IUserRepository : IRepository<User>
    {

    }

    public class UserRepository : IUserRepository
    {
        private readonly ApplicationDbContext _db;

        public UserRepository(ApplicationDbContext db)
        {
            _db = db;
        }

        public async Task<User> Add(User entity, CancellationToken cancellationToken)
        {
            var user = await _db.Users.AddAsync(entity, cancellationToken);

            await _db.SaveChangesAsync(cancellationToken);

            return user.Entity;
        }

        public async Task Delete(Guid id, CancellationToken cancellationToken)
        {
            var user = await _db.Users.FirstOrDefaultAsync(x => x.Id == id, cancellationToken);

            if (user != null)
            {
                _db.Users.Remove(user);

                await _db.SaveChangesAsync(cancellationToken);
            }
        }

        public async Task<IEnumerable<User>> Get(CancellationToken cancellationToken)
        {
            return await _db.Users.Include(x => x.QRCode).Include(x => x.EventUsers).ToListAsync(cancellationToken);
        }

        public async Task<User> GetById(Guid id, CancellationToken cancellationToken)
        {
            return await _db.Users.Include(x => x.QRCode).Include(x => x.EventUsers).FirstOrDefaultAsync(x => x.Id == id, cancellationToken);
        }

        public async Task Update(User entity, CancellationToken cancellationToken)
        {
            _db.Set<User>().Update(entity);

            await _db.SaveChangesAsync(cancellationToken);
        }
    }
}
