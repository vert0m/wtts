﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WTTS.Api.DataAccess;
using WTTS.Api.Entities;

namespace WTTS.Api.Repositories
{
    public interface IRegisterRepository : IRepository<Register>
    {

    }

    public class RegisterRepository : IRegisterRepository
    {
        private readonly ApplicationDbContext _db;

        public RegisterRepository(ApplicationDbContext db)
        {
            _db = db;
        }

        public async Task<Register> Add(Register entity, CancellationToken cancellationToken)
        {
            var record = await _db.Register.AddAsync(entity, cancellationToken);

            await _db.SaveChangesAsync(cancellationToken);

            return record.Entity;
        }

        public async Task Delete(Guid id, CancellationToken cancellationToken)
        {
            var record = await _db.Register.FirstOrDefaultAsync(x => x.Id == id, cancellationToken);

            if (record != null)
            {
                _db.Register.Remove(record);

                await _db.SaveChangesAsync(cancellationToken);
            }
        }

        public async Task<IEnumerable<Register>> Get(CancellationToken cancellationToken)
        {
            return await _db.Register.ToListAsync(cancellationToken);
        }

        public async Task<Register> GetById(Guid id, CancellationToken cancellationToken)
        {
            return await _db.Register.FirstOrDefaultAsync(x => x.Id == id, cancellationToken);
        }

        public async Task Update(Register entity, CancellationToken cancellationToken)
        {
            _db.Set<Register>().Update(entity);

            await _db.SaveChangesAsync(cancellationToken);
        }
    }
}
