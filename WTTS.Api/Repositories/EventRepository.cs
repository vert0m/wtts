﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WTTS.Api.DataAccess;
using WTTS.Api.Entities;

namespace WTTS.Api.Repositories
{
    public interface IEventRepository : IRepository<Event>
    {

    }

    public class EventRepository : IEventRepository
    {
        private readonly ApplicationDbContext _db;

        public EventRepository(ApplicationDbContext db)
        {
            _db = db;
        }

        public async Task<Event> Add(Event entity, CancellationToken cancellationToken)
        {
            var @event = await _db.Events.AddAsync(entity, cancellationToken);

            await _db.SaveChangesAsync(cancellationToken);

            return @event.Entity;
        }

        public async Task Delete(Guid id, CancellationToken cancellationToken)
        {
            var @event = await _db.Events.FirstOrDefaultAsync(x => x.Id == id, cancellationToken);

            if (@event != null)
            {
                _db.Events.Remove(@event);

                await _db.SaveChangesAsync(cancellationToken);
            }
        }

        public async Task<IEnumerable<Event>> Get(CancellationToken cancellationToken)
        {
            return await _db.Events.Include(x => x.QRCode).Include(x => x.EventUsers).ThenInclude(x => x.User).ToListAsync(cancellationToken);
        }

        public async Task<Event> GetById(Guid id, CancellationToken cancellationToken)
        {
            return await _db.Events.Include(x => x.QRCode).Include(x => x.EventUsers).ThenInclude(x => x.User).FirstOrDefaultAsync(x => x.Id == id, cancellationToken);
        }

        public async Task Update(Event entity, CancellationToken cancellationToken)
        {
            _db.Set<Event>().Update(entity);

            await _db.SaveChangesAsync(cancellationToken);
        }
    }
}
