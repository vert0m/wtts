﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WTTS.Api.DataAccess;
using WTTS.Api.Entities;

namespace WTTS.Api.Repositories
{
    public interface IQRCodeRepository : IRepository<QRCode>
    {

    }

    public class QRCodeRepository : IQRCodeRepository
    {
        private readonly ApplicationDbContext _db;

        public QRCodeRepository(ApplicationDbContext db)
        {
            _db = db;
        }

        public async Task<QRCode> Add(QRCode entity, CancellationToken cancellationToken)
        {
            var code = await _db.QRCodes.AddAsync(entity, cancellationToken);

            await _db.SaveChangesAsync(cancellationToken);

            return code.Entity;
        }

        public async Task Delete(Guid id, CancellationToken cancellationToken)
        {
            var code = await _db.QRCodes.FirstOrDefaultAsync(x => x.Id == id, cancellationToken);

            if (code != null)
            {
                _db.QRCodes.Remove(code);

                await _db.SaveChangesAsync(cancellationToken);
            }
        }

        public async Task<IEnumerable<QRCode>> Get(CancellationToken cancellationToken)
        {
            return await _db.QRCodes.ToListAsync(cancellationToken);
        }

        public async Task<QRCode> GetById(Guid id, CancellationToken cancellationToken)
        {
            return await _db.QRCodes.FirstOrDefaultAsync(x => x.Id == id, cancellationToken);
        }

        public async Task Update(QRCode entity, CancellationToken cancellationToken)
        {
            _db.Set<QRCode>().Update(entity);

            await _db.SaveChangesAsync(cancellationToken);
        }
    }
}
