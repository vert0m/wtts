﻿using System;

namespace WTTS.Api.Enums
{
    public enum UserRole
    {
        Admin,
        User
    }
}
