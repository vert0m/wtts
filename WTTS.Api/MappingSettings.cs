﻿using AutoMapper;
using WTTS.Api.Entities;

namespace WTTS.Api
{
    public class MappingSettings : Profile
    {
        public MappingSettings()
        {
            CreateMap<User, Models.UserDTO>();
            CreateMap<Models.UserDTO, User>();

            CreateMap<Event, Models.EventDTO>();
            CreateMap<Models.EventDTO, Event>();

            CreateMap<Register, Models.RegisterDTO>();
            CreateMap<Models.RegisterDTO, Register>();
        }
    }
}
