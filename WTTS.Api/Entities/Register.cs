﻿using System;

namespace WTTS.Api.Entities
{
    public class Register
    {
        public Register()
        {
            Timestamp = DateTime.Now;
        }

        public Guid Id { get; set; }

        public DateTime Timestamp { get; set; }

        public Guid User { get; set; }

        public Guid Event { get; set; }
    }
}
