﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using WTTS.Api.Enums;

namespace WTTS.Api.Entities
{
    public class User
    {
        public User()
        {
            Role = UserRole.User;

            CreateDate = DateTime.Now;
            UpdateDate = DateTime.Now;

            EventUsers = new List<EventUser>();
        }

        public Guid Id { get; set; }

        [Required, MaxLength(30)]
        public string FirstName { get; set; }

        [Required, MaxLength(30)]
        public string SecondName { get; set; }

        [Required, EmailAddress]
        public string Email { get; set; }

        public UserRole Role { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime UpdateDate { get; set; }

        public QRCode QRCode { get; set; }

        public List<EventUser> EventUsers { get; set; }
    }
}
