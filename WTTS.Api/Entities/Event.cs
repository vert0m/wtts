﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WTTS.Api.Entities
{
    public class Event
    {
        public Event()
        {
            CreateDate = DateTime.Now;
            UpdateDate = DateTime.Now;

            EventUsers = new List<EventUser>();
        }

        public Guid Id { get; set; }

        [Required, MaxLength(30)]
        public string Name { get; set; }

        public string Description { get; set; }

        [Required]
        public string Location { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime UpdateDate { get; set; }

        public QRCode QRCode { get; set; }

        public List<EventUser> EventUsers { get; set; }
    }
}
