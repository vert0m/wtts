﻿using System;

namespace WTTS.Api.Entities
{
    public class EventUser
    {
        public Guid Id { get; set; }

        public Guid UserId { get; set; }
        public User User { get; set; }

        public Guid EventId { get; set; }
        public Event Event { get; set; }
    }
}
