﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace WTTS.Api.Entities
{
    public class QRCode
    {
        public QRCode()
        {
            Code = Guid.NewGuid();

            CreateDate = DateTime.Now;
            UpdateDate = DateTime.Now;
        }

        public Guid Id { get; set; }

        public Guid Code { get; set; }

        public string Data { get; set; }

        public string PathToImage { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime UpdateDate { get; set; }
    }
}
