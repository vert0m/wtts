﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using WTTS.Api.Entities;
using WTTS.Api.Repositories;
using WTTS.QRCodeUtility;

namespace WTTS.Api.Services
{
    public interface IUserService
    {
        Task<User> Create(User user, CancellationToken cancellationToken);
    }

    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly IQRCodeRepository _qRCodeRepository;

        private readonly IConfiguration _config;

        public UserService(IUserRepository userRepository, IQRCodeRepository qRCodeRepository, IConfiguration config)
        {
            _userRepository = userRepository;
            _qRCodeRepository = qRCodeRepository;

            _config = config;
        }

        public async Task<User> Create(User user, CancellationToken cancellationToken)
        {
            var qrCode = await _qRCodeRepository.Add(new QRCode(), cancellationToken);

            user.QRCode = qrCode;

            var configSection = _config.GetSection("Consts:QRCodeImage");

            var qrCodeHelper = new QRCodeHelper(configSection.GetValue<int>("Height"), configSection.GetValue<int>("Width"),
                                     configSection.GetValue<int>("Margin"), Path.Combine(_config.GetSection("Consts")["PathToStorage"], "users"));

            user.QRCode.PathToImage = qrCodeHelper.Encode(qrCode.Code);

            return await _userRepository.Add(user, cancellationToken);
        }
    }
}
