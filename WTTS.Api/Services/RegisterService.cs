﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using WTTS.Api.Models;
using WTTS.Api.Repositories;

namespace WTTS.Api.Services
{
    public interface IRegisterService
    {
        Task<IEnumerable<Statistic>> GetMonthStatisticByUserId(Guid id, CancellationToken cancellationToken);
    }

    public class RegisterService : IRegisterService
    {
        private readonly IUserRepository _userRepository;
        private readonly IRegisterRepository _registerRepository;

        private readonly IConfiguration _config;

        public RegisterService(IUserRepository userRepository, IRegisterRepository registerRepository, IConfiguration config)
        {
            _userRepository = userRepository;
            _registerRepository = registerRepository;

            _config = config;
        }

        public async Task<IEnumerable<Statistic>> GetMonthStatisticByUserId(Guid id, CancellationToken cancellationToken)
        {
            var startDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0).AddMonths(-1);
            var endDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);

            var response = new List<Statistic>();

            var registerRecords = await _registerRepository.Get(cancellationToken);

            while (startDate <= endDate)
            {
                var endDate2 = startDate.AddDays(1);

                var filteredRecords = registerRecords
                    .Where(x => x.User == id &&
                        x.Timestamp >= startDate &&
                        x.Timestamp <= endDate2)
                    .OrderByDescending(x => x.Timestamp).ToList();

                if (filteredRecords.LastOrDefault() != null &&
                    filteredRecords.FirstOrDefault() != null)
                {
                    response.Add(new Statistic
                    {
                        Timestamp = startDate.ToShortDateString(),
                        Value = (filteredRecords.FirstOrDefault().Timestamp - filteredRecords.LastOrDefault().Timestamp).Hours
                    });
                }

                startDate = startDate.AddDays(1);
            }

            return response;
        }
    }
}
