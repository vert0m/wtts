﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using WTTS.Api.Entities;
using WTTS.Api.Repositories;
using WTTS.QRCodeUtility;

namespace WTTS.Api.Services
{
    public interface IEventService
    {
        Task<Event> Create(Event @event, string[] usersIds, CancellationToken cancellationToken);
    }

    public class EventService : IEventService
    {
        private readonly IEventRepository _eventRepository;
        private readonly IUserRepository _userRepository;
        private readonly IQRCodeRepository _qRCodeRepository;

        private readonly IConfiguration _config;

        public EventService(IEventRepository eventRepository, IQRCodeRepository qRCodeRepository, IUserRepository userRepository, IConfiguration config)
        {
            _eventRepository = eventRepository;
            _userRepository = userRepository;
            _qRCodeRepository = qRCodeRepository;

            _config = config;
        }

        public async Task<Event> Create(Event @event, string[] usersIds, CancellationToken cancellationToken)
        {
            foreach (var id in usersIds)
            {
                var user = await _userRepository.GetById(Guid.Parse(id), cancellationToken);

                if (user != null)
                {
                    @event.EventUsers.Add(new EventUser { EventId = @event.Id, UserId = user.Id });
                }
            }

            var qrCode = await _qRCodeRepository.Add(new QRCode(), cancellationToken);

            @event.QRCode = qrCode;

            var configSection = _config.GetSection("Consts:QRCodeImage");

            var qrCodeHelper = new QRCodeHelper(configSection.GetValue<int>("Height"), configSection.GetValue<int>("Width"),
                                     configSection.GetValue<int>("Margin"), Path.Combine(_config.GetSection("Consts")["PathToStorage"], "events"));

            @event.QRCode.PathToImage = qrCodeHelper.Encode(qrCode.Code);

            return await _eventRepository.Add(@event, cancellationToken);
        }
    }
}
