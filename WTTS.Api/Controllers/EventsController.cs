﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WTTS.Api.Repositories;
using WTTS.Api.Entities;
using AutoMapper;
using WTTS.Api.Services;
using System.IO;
using Microsoft.Extensions.Configuration;

namespace WTTS.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EventsController : ControllerBase
    {
        private readonly IEventRepository _eventRepository;
        private readonly IEventService _eventService;

        private readonly IMapper _mapper;

        private readonly IConfiguration _config;

        public EventsController(IEventRepository eventRepository, IEventService eventService, IMapper mapper, IConfiguration config)
        {
            _eventRepository = eventRepository;
            _eventService = eventService;

            _mapper = mapper;

            _config = config;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Models.EventDTO>>> Get(CancellationToken cancellationToken)
        {
            var response = await _eventRepository.Get(cancellationToken);

            return response != null ?
                (ActionResult<IEnumerable<Models.EventDTO>>)Ok(_mapper.Map<IEnumerable<Models.EventDTO>>(response)) :
                (ActionResult<IEnumerable<Models.EventDTO>>)NotFound();
        }

        [HttpGet("{id}/image")]
        public async Task<ActionResult> GetQRCodeImageByEventId(Guid id, CancellationToken cancellationToken)
        {
            var @event = await _eventRepository.GetById(id, cancellationToken);

            var image = System.IO.File.OpenRead(Path.Combine(_config.GetSection("Consts")["PathToStorage"], "events", @event.QRCode.PathToImage));
            return File(image, "image/jpeg");
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Models.EventDTO>> GetById(Guid id, CancellationToken cancellationToken)
        {
            if (!Guid.TryParse(id.ToString(), out var parsedId))
            {
                return BadRequest(parsedId);
            }

            var response = await _eventRepository.GetById(id, cancellationToken);

            return response != null ?
                (ActionResult<Models.EventDTO>)Ok(_mapper.Map<Models.EventDTO>(response)) :
                (ActionResult<Models.EventDTO>)NotFound();
        }

        [HttpPost]
        public async Task<ActionResult<Models.EventDTO>> Create([FromBody] Event @event, [FromQuery(Name = "ids")] string[] ids, CancellationToken cancellationToken)
        {
            if (!ModelState.IsValid || @event == null)
            {
                return BadRequest();
            }

            var response = await _eventService.Create(@event, ids, cancellationToken);

            return Ok(_mapper.Map<Models.EventDTO>(response));
        }

        [HttpPut]
        public async Task<ActionResult<Models.EventDTO>> Update([FromBody] Event @event, CancellationToken cancellationToken)
        {
            if (!ModelState.IsValid || @event == null)
            {
                return BadRequest();
            }

            await _eventRepository.Update(@event, cancellationToken);

            return Ok();
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(Guid id, CancellationToken cancellationToken)
        {
            if (!Guid.TryParse(id.ToString(), out var parsedId))
            {
                return BadRequest(parsedId);
            }

            _eventRepository.Delete(id, cancellationToken);

            return Ok();
        }
    }
}
