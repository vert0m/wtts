﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WTTS.Api.Repositories;
using WTTS.Api.Services;
using WTTS.Api.Entities;
using AutoMapper;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.SignalR;
using System.Linq;

namespace WTTS.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RegisterController : ControllerBase
    {
        private readonly IRegisterRepository _registerRepository;

        private readonly IRegisterService _registerService;

        private readonly IHubContext<RegisterHub> _hubContext;

        private readonly IMapper _mapper;

        private readonly IConfiguration _config;

        public RegisterController(IRegisterRepository registerRepository, IRegisterService registerService, IHubContext<RegisterHub> hubContext, IMapper mapper, IConfiguration config)
        {
            _registerRepository = registerRepository;

            _registerService = registerService;

            _hubContext = hubContext;

            _mapper = mapper;

            _config = config;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Models.RegisterDTO>>> Get(CancellationToken cancellationToken)
        {
            var response = await _registerRepository.Get(cancellationToken);

            return response != null ? 
                (ActionResult<IEnumerable<Models.RegisterDTO>>)Ok(_mapper.Map<IEnumerable<Models.RegisterDTO>>(response.OrderByDescending(x => x.Timestamp))) : 
                (ActionResult<IEnumerable<Models.RegisterDTO>>)NotFound();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Models.RegisterDTO>> GetById(Guid id, CancellationToken cancellationToken)
        {
            if (!Guid.TryParse(id.ToString(), out var parsedId))
            {
                return BadRequest(parsedId);
            }

            var response = await _registerRepository.GetById(id, cancellationToken);

            return response != null ?
                (ActionResult<Models.RegisterDTO>)Ok(_mapper.Map<Models.RegisterDTO>(response)) :
                (ActionResult<Models.RegisterDTO>)NotFound();
        }

        [HttpGet("{id}/statistic")]
        public async Task<ActionResult<Models.Statistic>> GetStatisticById(Guid id, CancellationToken cancellationToken)
        {
            if (!Guid.TryParse(id.ToString(), out var parsedId))
            {
                return BadRequest(parsedId);
            }

            var response = await _registerService.GetMonthStatisticByUserId(id, cancellationToken);

            return response != null ?
                (ActionResult<Models.Statistic>)Ok(response) :
                (ActionResult<Models.Statistic>)NotFound();
        }

        [HttpPost]
        public async Task<ActionResult<Models.RegisterDTO>> Create([FromBody] Register record, CancellationToken cancellationToken)
        {
            if (!ModelState.IsValid || record == null)
            {
                return BadRequest();
            }

            var response = await _registerRepository.Add(record, cancellationToken);
            var records = await _registerRepository.Get(cancellationToken);

            await _hubContext.Clients.All.SendAsync("Broadcast", records.OrderByDescending(x => x.Timestamp));

            return Ok(_mapper.Map<Models.RegisterDTO>(response));
        }

        [HttpPut]
        public async Task<ActionResult<Models.RegisterDTO>> Update([FromBody] Register record, CancellationToken cancellationToken)
        {
            if (!ModelState.IsValid || record == null)
            {
                return BadRequest();
            }

            await _registerRepository.Update(record, cancellationToken);

            return Ok();
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(Guid id, CancellationToken cancellationToken)
        {
            if (!Guid.TryParse(id.ToString(), out var parsedId))
            {
                return BadRequest(parsedId);
            }

            _registerRepository.Delete(parsedId, cancellationToken);

            return Ok();
        }
    }
}
