﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WTTS.Api.Repositories;
using WTTS.Api.Services;
using WTTS.Api.Entities;
using AutoMapper;
using System.IO;
using Microsoft.Extensions.Configuration;

namespace WTTS.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUserRepository _userRepository;
        private readonly IUserService _userService;

        private readonly IMapper _mapper;

        private readonly IConfiguration _config;

        public UsersController(IUserRepository userRepository, IUserService userService, IMapper mapper, IConfiguration config)
        {
            _userRepository = userRepository;
            _userService = userService;

            _mapper = mapper;

            _config = config;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Models.UserDTO>>> Get(CancellationToken cancellationToken)
        {
            var response = await _userRepository.Get(cancellationToken);

            return response != null ? 
                (ActionResult<IEnumerable<Models.UserDTO>>)Ok(_mapper.Map<IEnumerable<Models.UserDTO>>(response)) : 
                (ActionResult<IEnumerable<Models.UserDTO>>)NotFound();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Models.UserDTO>> GetById(Guid id, CancellationToken cancellationToken)
        {
            if (!Guid.TryParse(id.ToString(), out var parsedId))
            {
                return BadRequest(parsedId);
            }

            var response = await _userRepository.GetById(id, cancellationToken);

            return response != null ?
                (ActionResult<Models.UserDTO>)Ok(_mapper.Map<Models.UserDTO>(response)) :
                (ActionResult<Models.UserDTO>)NotFound();
        }

        [HttpGet("{id}/image")]
        public async Task<ActionResult> GetQRCodeImageByUserId(Guid id, CancellationToken cancellationToken)
        {
            var user = await _userRepository.GetById(id, cancellationToken);

            var image = System.IO.File.OpenRead(Path.Combine(_config.GetSection("Consts")["PathToStorage"], "users", user.QRCode.PathToImage));
            return File(image, "image/jpeg");
        }

        [HttpPost]
        public async Task<ActionResult<Models.UserDTO>> Create([FromBody] User user, CancellationToken cancellationToken)
        {
            if (!ModelState.IsValid || user == null)
            {
                return BadRequest();
            }

            var response = await _userService.Create(user, cancellationToken);

            return Ok(_mapper.Map<Models.UserDTO>(response));
        }

        [HttpPut]
        public async Task<ActionResult<Models.UserDTO>> Update([FromBody] User user, CancellationToken cancellationToken)
        {
            if (!ModelState.IsValid || user == null)
            {
                return BadRequest();
            }

            await _userRepository.Update(user, cancellationToken);

            return Ok();
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(Guid id, CancellationToken cancellationToken)
        {
            if (!Guid.TryParse(id.ToString(), out var parsedId))
            {
                return BadRequest(parsedId);
            }

            _userRepository.Delete(id, cancellationToken);

            return Ok();
        }
    }
}
