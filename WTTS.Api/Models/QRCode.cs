﻿using System;

namespace WTTS.Api.Models
{
    public class QRCodeDTO
    {
        public Guid Id { get; set; }
        public Guid Code { get; set; }
        public string Data { get; set; }
        public string PathToImage { get; set; }
    }
}
