﻿using System;

namespace WTTS.Api.Models
{
    public class Statistic
    {   
        public string Timestamp { get; set; }
        public double Value { get; set; }
    }
}
