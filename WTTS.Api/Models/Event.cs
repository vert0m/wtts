﻿using System;
using System.Collections.Generic;

namespace WTTS.Api.Models
{
    public class EventDTO
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Location { get; set; }
        public QRCodeDTO QRCode { get; set; }
        public List<EventUserDTO> EventUsers { get; set; }
    }
}
