﻿using System;

namespace WTTS.Api.Models
{
    public class EventUserDTO
    {
        public Guid Id { get; set; }

        public Guid UserId { get; set; }
        public UserDTO User { get; set; }

        public Guid EventId { get; set; }
        public EventDTO Event { get; set; }
    }
}
