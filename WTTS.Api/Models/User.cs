﻿using System;
using System.Collections.Generic;
using WTTS.Api.Enums;

namespace WTTS.Api.Models
{
    public class UserDTO
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string Email { get; set; }
        public UserRole Role { get; set; }
        public QRCodeDTO QRCode { get; set; }
        public List<EventUserDTO> EventUsers { get; set; }
    }
}
