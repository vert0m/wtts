﻿using System;

namespace WTTS.Api.Models
{
    public class RegisterDTO
    {
        public Guid Id { get; set; }
        public DateTime Timestamp { get; set; }
        public Guid User { get; set; }
        public Guid Event { get; set; }
    }
}
