﻿using Microsoft.EntityFrameworkCore;
using WTTS.Api.Entities;

namespace WTTS.Api.DataAccess
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        { }

        public DbSet<User> Users { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<QRCode> QRCodes { get; set; }
        public DbSet<Register> Register { get; set; }

    }
}
