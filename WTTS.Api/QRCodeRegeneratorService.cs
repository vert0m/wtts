﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WTTS.Api.DataAccess;
using WTTS.Api.Entities;
using WTTS.QRCodeUtility;

namespace WTTS.Api
{
    internal class QRCodeRegeneratorService : IHostedService, IDisposable
    {
        private Timer _timer;
        private readonly IConfiguration _config;

        public QRCodeRegeneratorService(IConfiguration config)
        {
            _config = config;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _timer = new Timer(Regenerate, null, TimeSpan.Zero, TimeSpan.FromSeconds(_config.GetSection("Consts").GetValue<int>("RegenerateInterval")));

            return Task.CompletedTask;
        }

        private void Regenerate(object state)
        {
            var optionsBuilder = new DbContextOptionsBuilder<ApplicationDbContext>();
            optionsBuilder.UseSqlite(_config.GetConnectionString("SQLite"));

            using (var _db = new ApplicationDbContext(optionsBuilder.Options))
            {
                var events = _db.Events.Include(x => x.QRCode).ToList();


                events.ForEach(x => {
                    // todo

                    //_db.QRCodes.Remove(x.QRCode);
                    //Directory.Delete(Path.Combine(_config.GetSection("Consts")["PathToStorage"], "events", x.QRCode.Code.ToString()), true);
                    x.QRCode = new QRCode();
                });

                _db.SaveChanges();

                var configSection = _config.GetSection("Consts:QRCodeImage");

                var qrCodeHelper = new QRCodeHelper(configSection.GetValue<int>("Height"), configSection.GetValue<int>("Width"),
                                            configSection.GetValue<int>("Margin"), Path.Combine(_config.GetSection("Consts")["PathToStorage"], "events"));

                events.ForEach(x => x.QRCode.PathToImage = qrCodeHelper.Encode(x.QRCode.Code));

                _db.SaveChanges();

            }
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _timer?.Dispose();
        }
    }
}
