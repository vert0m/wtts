﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;
using ZXing;
using ZXing.QrCode;

namespace WTTS.QRCodeUtility
{
    public class QRCodeHelper
    {
        private readonly int _width;
        private readonly int _height;
        private readonly int _margin;
        private readonly string _pathToFolder;

        public QRCodeHelper(int width, int height, int margin, string pathToFolder)
        {
            _width = width;
            _height = height;
            _margin = margin;

            _pathToFolder = pathToFolder;
        }

        public string Decode(string pathToCode)
        {
            var barcodeBitmap = Image.FromFile(Path.Combine(_pathToFolder, pathToCode));

            var reader = new BarcodeReader { AutoRotate = true };

            using (var ms = new MemoryStream())
            {
                barcodeBitmap.Save(ms, ImageFormat.Bmp);

                Result result = reader.Decode(ms.ToArray(), _width, _height, RGBLuminanceSource.BitmapFormat.RGB32);

                return result.Text;
            }
        }

        public string Encode(Guid code)
        {
            var qrCodeWriter = new BarcodeWriterPixelData
            {
                Format = BarcodeFormat.QR_CODE,
                Options = new QrCodeEncodingOptions
                {
                    Height = _height,
                    Width = _width,
                    Margin = _margin
                }
            };

            var pixelData = qrCodeWriter.Write(code.ToString());

            // creating a bitmap from the raw pixel data; if only black and white colors are used it makes no difference    
            // that the pixel data ist BGRA oriented and the bitmap is initialized with RGB    
            using (var bitmap = new Bitmap(pixelData.Width, pixelData.Height, PixelFormat.Format32bppRgb))
            using (var ms = new MemoryStream())
            {
                var bitmapData = bitmap.LockBits(new Rectangle(0, 0, pixelData.Width, pixelData.Height), ImageLockMode.WriteOnly, PixelFormat.Format32bppRgb);
                try
                {
                    // we assume that the row stride of the bitmap is aligned to 4 byte multiplied by the width of the image    
                    Marshal.Copy(pixelData.Pixels, 0, bitmapData.Scan0, pixelData.Pixels.Length);
                }
                finally
                {
                    bitmap.UnlockBits(bitmapData);
                }

                Directory.CreateDirectory(Path.Combine(_pathToFolder, code.ToString()));

                // save to stream as PNG    
                bitmap.Save(Path.Combine(_pathToFolder, code.ToString(), code.ToString() + ".png"), ImageFormat.Png);

                return Path.Combine(code.ToString(), code.ToString() + ".png");
            }
        }
    }
}
