﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ZXing;

namespace WTTS.DesktopClient
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var fileDialog = new System.Windows.Forms.OpenFileDialog();
            var result = fileDialog.ShowDialog();

            switch (result)
            {
                case System.Windows.Forms.DialogResult.OK:
                    var id = this.Decode(fileDialog.FileName);
                    System.Windows.Forms.MessageBox.Show(id);
                    SendReq("112e432b-a34b-4483-b57a-0d65f2f001d2", id);
                    break;
                case System.Windows.Forms.DialogResult.Cancel:
                default:
                    break;
            }
        }

        public static async Task<string> SendReq(string personId, string eventId)
        {
            using (var client = new HttpClient())
            {
                var content = new StringContent($"{{'user': '{personId}', 'event': '{eventId}'}}".ToString(), Encoding.UTF8, "application/json");
                var result = await client.PostAsync("http://localhost:5010/api/register", content);
                System.Windows.Forms.MessageBox.Show(result.Content.ToString());
                return result.Content.ToString();
            }
        }

        public string Decode(string pathToCode)
        {
            var reader = new BarcodeReader { AutoRotate = true };

            Bitmap bmp = new Bitmap(pathToCode);

            Result result = reader.Decode(bmp);

            return result.Text;
        }
    }
}
