export const environment = {
  production: true,
  apiUrl: 'http://localhost:5010/api/',
  registryBroadcastUrl: 'http://localhost:5010/registerhub'
};
