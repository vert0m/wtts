export class Statistic {
    constructor(
        public timestamp: string,
        public value: number) { }
}
