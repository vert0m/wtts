import { User } from './user';
import { Event } from './event';

export class EventUsers {
    constructor(
    public id: string,
    public userId: string,
    public user: User,
    public eventId: string,
    public event: Event) { }
}
