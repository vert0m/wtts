export class EventRecord {
    constructor(
    public id: string,
    public timestamp: string,
    public userId: string,
    public eventId: string) { }
}

//export class EventModel {
//  constructor(
//    public name: string,
//    public description: string,
//    public location: string) { }
//}
