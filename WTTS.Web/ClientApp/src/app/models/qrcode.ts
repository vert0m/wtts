export class QRCode {
    constructor(
      public id:	string,
      public code:	string,
      public data:	string,
      public pathToImage:	string
    ) { }
}

export class QRCodeImage {
  constructor(
    public id:	string,
    public image:	string
  ) { }
}
