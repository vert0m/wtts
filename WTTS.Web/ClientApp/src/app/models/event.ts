import { QRCode } from './qrcode';
import { EventUsers } from './eventUsers';

export class Event {
    constructor(
    public id: string,
    public name: string,
    public description: string,
    public location: string,
    public qrCode: QRCode,
    public eventUsers: EventUsers[]) { }
}

export class EventModel {
  constructor(
    public name: string,
    public description: string,
    public location: string) { }
}
