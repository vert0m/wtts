import { QRCode } from './qrcode';
import { EventUsers } from './eventUsers';

export class User {
    constructor(
        public id: string,
        public firstName: string,
        public secondName: string,
        public email: string,
        public role: number,
        public qrCode: QRCode,
        public eventUsers: EventUsers[]) { }
}

export class UserModel {
  constructor(
    public firstName: string,
    public secondName: string,
    public email: string,
    public role: number
  ) { }
}
