import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User, UserModel } from 'src/app/models/user';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private http: HttpClient) { }

  getUsers(): Observable<User[]> {
    return this.http.get<User[]>(environment.apiUrl + `users`);
  }

  getUsersById(id: string): Observable<User> {
    return this.http.get<User>(environment.apiUrl + `users/${id}`);
  }

  addUser(user: UserModel): Observable<User> {
    return this.http.post<User>(environment.apiUrl + `users`, user);
  }
}
