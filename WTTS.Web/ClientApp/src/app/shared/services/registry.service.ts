import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { EventRecord } from '../../models/record';
import * as signalR from '@aspnet/signalr';
import { HubConnection } from '@aspnet/signalr';
import { Statistic } from '../../models/statistic';

@Injectable({
  providedIn: 'root'
})
export class RegistryService {
  public connection: HubConnection;

  constructor(private http: HttpClient) { }

  getRecords(): Observable<EventRecord[]> {
    return this.http.get<EventRecord[]>(environment.apiUrl + `register`);
  }

  getUserStatisticById(id: string): Observable<Statistic[]> {
    return this.http.get<Statistic[]>(environment.apiUrl + `register/${id}/statistic`);
  }

  // signalR
  startConnection() {
    this.connection = new signalR.HubConnectionBuilder()
      .withUrl(environment.registryBroadcastUrl)
      .build();

    return this.connection.start();
  }

  closeConnection() {
    this.connection.stop();
  }
}
