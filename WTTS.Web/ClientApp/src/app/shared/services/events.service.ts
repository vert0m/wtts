import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';
import { Event, EventModel } from '../../models/event';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EventsService {
  constructor(private http: HttpClient) { }

  getEvents(): Observable<Event[]> {
    return this.http.get<Event[]>(environment.apiUrl + `events`);
  }

  addEvent(event: EventModel, ids: string[]): Observable<Event> {
    let queryString = '?';

    for (let id of ids) {
      queryString += `ids=${id}&`;
    }

    return this.http.post<Event>(environment.apiUrl + `events${queryString}`, event);
  }
}
