import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EventsCreateComponent } from './events-create/events-create.component';
import { EventsListComponent } from './events-list/events-list.component';

const routes: Routes = [
  {
    path: '', component: EventsListComponent
  },
  {
    path: 'create', component: EventsCreateComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EventsRoutingModule { }
