import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { EventsService } from '../../../shared/services/events.service';
import { EventModel } from '../../../models/event';
import { User } from '../../../models/user';
import { Router } from '@angular/router';
import { UsersService } from '../../../shared/services/users.service';

@Component({
  selector: 'app-events-create',
  templateUrl: './events-create.component.html',
  styleUrls: ['./events-create.component.css']
})
export class EventsCreateComponent implements OnInit {
  name = new FormControl('', [Validators.required]);
  description = new FormControl('');
  location = new FormControl('', [Validators.required]);

  users = new FormControl('', [Validators.required]);
  usersList: User[];

  constructor(private eventsService: EventsService, private usersService: UsersService, private router: Router) { }

  ngOnInit() {
    this.usersService.getUsers()
      .subscribe(users => this.usersList = users);
  }

  create(): void {
    if (this.name.valid && this.users.valid) {
      let choosenUsers = this.users.value as User[];
      let ids = choosenUsers.map(user => user.id);

      this.eventsService.addEvent(new EventModel(this.name.value, this.description.value, this.location.value), ids)
        .subscribe(() => this.router.navigate(['/events']));
    } else {
      this.name.markAsTouched();
      this.users.markAsTouched();
      return;
    }
  }

  validateName() {
    return this.name.hasError('required') ? 'You must enter a value' : '';
  }

  validateLocation() {
    return this.location.hasError('required') ? 'You must enter a value' : '';
  }

  validateUsers() {
    return this.users.hasError('required') ? 'You must enter a value' : '';
  }
}
