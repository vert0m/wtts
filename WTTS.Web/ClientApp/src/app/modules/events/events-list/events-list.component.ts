import { Component, OnInit } from '@angular/core';
import { EventsService } from '../../../shared/services/events.service';
import { Event } from '../../../models/event';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-events-list',
  templateUrl: './events-list.component.html',
  styleUrls: ['./events-list.component.css']
})
export class EventsListComponent implements OnInit {
  events: any[];
  private fullscreenImage: boolean;

  constructor(private eventsService: EventsService) {
    this.fullscreenImage = false;
  }

  ngOnInit() {
    this.eventsService.getEvents()
      .subscribe((events: Event[]) =>
        this.events = events.map(e =>
          ({
            id: e.id,
            name: e.name,
            description: e.description,
            location: e.location,
            users: e.eventUsers,
            image: environment.apiUrl + `events/${e.id}/image`
          })));
  }

  openImage(): void {
    this.fullscreenImage = !this.fullscreenImage;
  }
}
