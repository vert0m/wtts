import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EventsRoutingModule } from './events-routing.module';
import { EventsCreateComponent } from './events-create/events-create.component';
import {
  MatButtonModule,
  MatFormFieldModule,
  MatInputModule,
  MatOptionModule,
  MatSelectModule,
  MatExpansionModule,
  MatTableModule,
  MatListModule,
  MatIconModule
} from '@angular/material';
import { ReactiveFormsModule } from '@angular/forms';
import { EventsService } from '../../shared/services/events.service';
import { EventsListComponent } from './events-list/events-list.component';

@NgModule({
  declarations: [EventsCreateComponent, EventsListComponent],
  imports: [
    CommonModule,
    EventsRoutingModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule,
    MatOptionModule,
    MatSelectModule,
    MatExpansionModule,
    MatTableModule,
    MatListModule,
    MatIconModule
  ],
  providers: [EventsService]
})
export class EventsModule { }
