import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersRoutingModule } from './users-routing.module';
import { UsersListComponent } from './users-list/users-list.component';
import { UsersService } from 'src/app/shared/services/users.service';
import { MatTableModule, MatPaginatorModule, MatCardModule, MatButtonModule, MatFormFieldModule, MatInputModule } from '@angular/material';
import { UsersDetailsComponent } from './users-details/users-details.component';
import { UsersCreateComponent } from './users-create/users-create.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxChartsModule } from '@swimlane/ngx-charts';

@NgModule({
  declarations: [UsersListComponent, UsersDetailsComponent, UsersCreateComponent],
  imports: [
    CommonModule,
    UsersRoutingModule,
    MatTableModule,
    MatPaginatorModule,
    MatCardModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule,
    NgxChartsModule
  ],
  providers: [UsersService]
})
export class UsersModule { }
