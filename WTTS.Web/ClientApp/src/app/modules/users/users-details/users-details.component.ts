import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UsersService } from '../../../shared/services/users.service';
import { User } from '../../../models/user';
import { environment } from '../../../../environments/environment';
import { RegistryService } from '../../../shared/services/registry.service';
import { Statistic } from '../../../models/statistic';

@Component({
  selector: 'app-users-details',
  templateUrl: './users-details.component.html',
  styleUrls: ['./users-details.component.css']
})
export class UsersDetailsComponent implements OnInit {
  user: User;
  pathToImage: string;

  single: any[];

  view: any[] = [400, 300];

  // options
  showXAxis = true;
  showYAxis = true;
  gradient = true;
  showLegend = true;
  showXAxisLabel = true;
  xAxisLabel = 'Time';
  showYAxisLabel = true;
  yAxisLabel = 'Date';

  colorScheme = {
    domain: ['#673ab7']
  };

  constructor(private route: ActivatedRoute, private usersService: UsersService, private registryService: RegistryService) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.pathToImage = environment.apiUrl + `users/${params['id']}/image`;

      this.usersService.getUsersById(params['id'])
        .subscribe(user => this.user = user);

      this.registryService.getUserStatisticById(params['id'])
        .subscribe((statistic: Statistic[]) => {
          this.single = statistic.map(s => ({ name: s.timestamp, value: s.value }));
        });
    });
  }

}
