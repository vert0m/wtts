import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsersListComponent } from './users-list/users-list.component';
import { UsersDetailsComponent } from './users-details/users-details.component';
import { UsersCreateComponent } from './users-create/users-create.component';

const routes: Routes = [
  {
    path: '', component: UsersListComponent
  },
  {
    path: 'create', component: UsersCreateComponent
  },
  {
    path: ':id', component: UsersDetailsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule { }
