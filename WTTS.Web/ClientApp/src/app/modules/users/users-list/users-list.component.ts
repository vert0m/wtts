import { Component, OnInit, ViewChild } from '@angular/core';
import { UsersService } from 'src/app/shared/services/users.service';
import { User } from 'src/app/models/user';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.css']
})
export class UsersListComponent implements OnInit {
  dataSource: MatTableDataSource<User>;
  tableColumns: string[] = ['Id', 'First Name', 'Second Name', 'Email'];

  filter = new FormControl('');

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private usersService: UsersService, private router: Router) { }

  ngOnInit() {
    this.usersService.getUsers()
      .subscribe(users => {
        this.dataSource = new MatTableDataSource<User>(users);
        this.dataSource.paginator = this.paginator;
      });

    this.filter.valueChanges
      .subscribe(name => {
          setTimeout(() => {
            this.dataSource.filter = name.trim().toLowerCase();
          }, 1000);
        }
      );
  }

  goToDetailsPage(id: string): void {
    this.router.navigate([`/users/${id}`]);
  }
}
