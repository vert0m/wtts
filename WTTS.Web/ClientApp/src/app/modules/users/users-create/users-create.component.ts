import { Component, OnInit } from '@angular/core';
import { Validators, FormControl } from '@angular/forms';
import { UserModel } from '../../../models/user';
import { UsersService } from '../../../shared/services/users.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-users-create',
  templateUrl: './users-create.component.html',
  styleUrls: ['./users-create.component.css']
})
export class UsersCreateComponent implements OnInit {
  firstName = new FormControl('', [Validators.required]);
  secondName = new FormControl('', [Validators.required]);
  email = new FormControl('', [Validators.required, Validators.email]);

  constructor(private usersService: UsersService, private router: Router) { }

  ngOnInit() {
  }

  create(): void {
    if (this.firstName.valid && this.secondName.valid && this.email.valid) {
      this.usersService.addUser(new UserModel(this.firstName.value, this.secondName.value, this.email.value, 1))
        .subscribe(() => this.router.navigate(['/users']));
    } else {
      this.firstName.markAsTouched();
      this.secondName.markAsTouched();
      this.email.markAsTouched();
      return;
    }
  }

  validateEmail() {
    return this.email.hasError('required') ? 'You must enter a value' :
        this.email.hasError('email') ? 'Not a valid email' : '';
  }

  validateFirstName() {
    return this.firstName.hasError('required') ? 'You must enter a First Name' : '';
  }

  validateSecondName() {
    return this.secondName.hasError('required') ? 'You must enter a Second Name' : '';
  }
}
