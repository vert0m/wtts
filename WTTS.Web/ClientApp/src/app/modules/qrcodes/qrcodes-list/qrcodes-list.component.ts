import { Component, OnInit } from '@angular/core';
import { UsersService } from '../../../shared/services/users.service';
import { environment } from '../../../../environments/environment';
import { QRCodeImage } from 'src/app/models/qrcode';

@Component({
  selector: 'app-qrcodes-list',
  templateUrl: './qrcodes-list.component.html',
  styleUrls: ['./qrcodes-list.component.css']
})
export class QrcodesListComponent implements OnInit {
  images: QRCodeImage[];

  constructor(private usersService: UsersService) { }

  ngOnInit() {
    this.usersService.getUsers()
      .subscribe(users => {
        this.images = users.map(user => new QRCodeImage(user.id, environment.apiUrl + `users/${user.id}/image`));
      });
  }

}
