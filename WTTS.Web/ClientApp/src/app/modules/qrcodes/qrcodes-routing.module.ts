import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { QrcodesListComponent } from './qrcodes-list/qrcodes-list.component';

const routes: Routes = [
  {
    path: '', component: QrcodesListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class QrcodesRoutingModule { }
