import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { QrcodesRoutingModule } from './qrcodes-routing.module';
import { QrcodesListComponent } from './qrcodes-list/qrcodes-list.component';
import { MatCardModule, MatButtonModule } from '@angular/material';

@NgModule({
  declarations: [QrcodesListComponent],
  imports: [
    CommonModule,
    QrcodesRoutingModule,
    MatCardModule,
    MatButtonModule
  ]
})
export class QrcodesModule { }
