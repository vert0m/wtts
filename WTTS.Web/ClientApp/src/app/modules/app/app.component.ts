import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(private router: Router) {
  }

  shouldShowAddButton(): boolean {
    if (this.router.url === '/' ||
      this.router.url === '/registry' ||
      this.router.url === '/qrcodes') {
      return false;
    }

    return true;
  }

  add(): void {
    if (this.router.url.includes('/users')) {
      this.router.navigate(['/users/create']);
    } else if (this.router.url.includes('/events')) {
      this.router.navigate(['/events/create']);
    } else {
      return;
    }
  }
}
