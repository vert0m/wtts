import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RegistryRoutingModule } from './registry-routing.module';
import { RegistryListComponent } from './registry-list/registry-list.component';
import { MatListModule } from '@angular/material';

@NgModule({
  declarations: [RegistryListComponent],
  imports: [
    CommonModule,
    RegistryRoutingModule,
    MatListModule
  ]
})
export class RegistryModule { }
