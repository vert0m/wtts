import { Component, OnInit, OnDestroy } from '@angular/core';
import { RegistryService } from '../../../shared/services/registry.service';
import { EventRecord } from '../../../models/record';

@Component({
  selector: 'app-registry-list',
  templateUrl: './registry-list.component.html',
  styleUrls: ['./registry-list.component.css']
})
export class RegistryListComponent implements OnInit, OnDestroy {
  records: EventRecord[];

  constructor(private registryService: RegistryService) { }

  ngOnInit() {
    this.registryService.getRecords()
      .subscribe(records => this.records = records);

    this.registryService
      .startConnection()
      .catch(error => console.log(error));

    this.registryService
      .connection
      .on('Broadcast', records => this.records = records);
  }

  recordDetails(record: EventRecord): void {
    console.log(record);
  }

  ngOnDestroy() {
    this.registryService.closeConnection();
  }
}
