import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegistryListComponent } from './registry-list/registry-list.component';

const routes: Routes = [
  { path: '', component: RegistryListComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RegistryRoutingModule { }
